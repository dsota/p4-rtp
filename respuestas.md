## Ejercicio 3. Análisis general
* ¿Cuántos paquetes componen la captura?: 1268
* ¿Cuánto tiempo dura la captura?: 12.810068 s
* Aproximadamente, ¿cuántos paquetes se envían por segundo?: 99 pps
* ¿Qué protocolos de nivel de red aparecen en la captura?: IPv4
* ¿Qué protocolos de nivel de transporte aparecen en la captura?: UDP
* ¿Qué protocolos de nivel de aplicación aparecen en la captura?: RTP
## Ejercicio 4. Análisis de un paquete
* Número aleatorio elegido (N): 327
* Dirección IP de la máquina A: 192.168.0.10
* Dirección IP de la máquina B: 216.234.64.16
* Puerto UDP desde el que se envía el paquete: 49154
* Puerto UDP hacia el que se envía el paquete: 54550
* SSRC del paquete: 0x2a173650 (706164304)
* Tipo de datos en el paquete RTP (según indica el campo correspondiente): ITU-T G.711 PCMU (0)
## Ejercicio 5. Análisis de un flujo (stream)
* ¿Cuál es el primer número de tu DNI / NIE?: 0
* ¿Cuántos paquetes hay en el flujo F?: 642
* ¿El origen del flujo F es la máquina A o B?: A
* ¿Cuál es el puerto UDP de destino del flujo F?: 54550
* ¿Cuántos segundos dura el flujo F?: 12.81s
## Ejercicio 6. Métricas del flujo
* ¿Cuál es la diferencia máxima en el flujo?: 21.187 ms
* ¿Cuál es el jitter medio del flujo?: 0.229065 ms
* ¿Cuántos paquetes del flujo se han perdido?: 0
* ¿Cuánto tiempo (aproximadamente) está el jitter por encima de 0.4 ms?: 500 ms
## Ejercicio 7. Métricas de un paquete RTP
* ¿Cuál es su número de secuencia dentro del flujo RTP?: 18599
* ¿Ha llegado pronto o tarde, con respecto a cuando habría debido llegar?: tarde
* ¿Qué jitter se ha calculado para ese paquete? 0.21948
* ¿Qué timestamp tiene ese paquete? 1769331723
* ¿Por qué número hexadecimal empieza sus datos de audio? f
## Ejercicio 8. Captura de una traza RTP
* Salida del comando `date`: sáb 21 oct 2023 13:18:06 CEST
* Número total de paquetes en la captura:frame.number == 903
* Duración total de la captura (en segundos):frame.time_relative == 3.174194264
## Ejercicio 9. Análisis de la captura realizada
* ¿Cuántos paquetes RTP hay en el flujo?: 450
* ¿Cuál es el SSRC del flujo?: 0xaf0709f5
* ¿En qué momento (en ms) de la traza comienza el flujo? 0.174989
* ¿Qué número de secuencia tiene el primer paquete del flujo? 26758
* ¿Cuál es el jitter medio del flujo?: 0
* ¿Cuántos paquetes del flujo se han perdido?: 0
## Ejercicio 10 (segundo periodo). Analiza la captura de un compañero
* ¿De quién es la captura que estás usando?: @d.santa.2021
* ¿Cuántos paquetes de más o de menos tiene que la tuya? (toda la captura): 1951.
* ¿Cuántos paquetes de más o de menos tiene el flujo RTP que el tuyo?: 0.
* ¿Qué diferencia hay entre el número de secuencia del primer paquete de su flujo RTP con respecto al tuyo?: 998.
* En general, ¿que diferencias encuentras entre su flujo RTP y el tuyo?: Su delta máximo es mayor que el de mi flujo RTP. Los identificadores SSRC son diferentes y tambien tanto el paquete como el tiempo de inicio del flujo.
